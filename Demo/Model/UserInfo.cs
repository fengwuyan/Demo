﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [DataContract]//数据契约
    public class UserInfo
    {
        [DataMember(IsRequired =true)]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public ushort Age { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public DateTime CreateTime { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }
    }
}
