﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [MessageContract]//消息契约
  public  class FileMessage
    {
        [MessageHeader]
        public string FilePath { get; set; }
        [MessageBodyMember]
        public FileStream FileData { get; set; }

    }
}
