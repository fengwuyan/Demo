﻿using IService;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class UserInfoService : IUserInfoService
    {
        public UserInfo[] GetAll()
        {
            List<UserInfo> list = new List<UserInfo>();
            list.Add(new UserInfo { Id=1, Age=18, Name="lm", Phone= "18479281670", IsDeleted=false, CreateTime=DateTime.Now });
            return list.ToArray();
        }
    }
}
