﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WcfClient.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%foreach (var item in userInfos)
                {%>
            <ul>
                <li><%=item.Id %></li>
                <li><%=item.Name %></li>
                <li><%=item.Age %></li>
                <li><%=item.CreateTime %></li>
                <li><%=item.IsDeleted %></li>
                <li><%=item.Phone %></li>
            </ul>
                
            <% } %>
        </div>
    </form>
</body>
</html>
