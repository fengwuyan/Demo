﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WcfClient
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected ServiceWcfOne.UserInfo[] userInfos;
        protected void Page_Load(object sender, EventArgs e)
        {
            ServiceWcfOne.UserInfoServiceClient client = new ServiceWcfOne.UserInfoServiceClient();
            userInfos = client.GetAll();
        }
    }
}