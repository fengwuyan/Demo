﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WevServiceTest
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TQYB.WeatherWSSoapClient client = new TQYB.WeatherWSSoapClient();
            GridView1.DataSource = client.getRegionDataset().Tables[0];
            GridView1.DataBind();
        }
    }
}