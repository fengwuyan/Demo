﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WevServiceTest
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected ServiceReference1.Product[] products;
        protected void Page_Load(object sender, EventArgs e)
        {
            ServiceReference1.WebServiceTestSoapClient client = new ServiceReference1.WebServiceTestSoapClient();
            products = client.GetAll();
        }
    }
}